import handlebars from 'handlebars';
import plugin from '../lib/plugins/index';

const hapi = require('hapi');

const serverConfig = require('./server.config');

/*eslint-disable*/
const mongodb = require("../lib/database/mongodb/index");
/* eslint-enable */

const starts = async () => {
  // server is started
  const server = new hapi.Server(serverConfig);

  // server plugin is registered
  await server.register(plugin);

  server.views({
    engines: {
      html: handlebars,
    },
    path: `${__dirname}/views`,
    layout: true,
  });

  // server is started
  await server.start();
  console.log(`Server is started at ${server.info.uri}`);
};

starts()
  .then(() => {
    console.log('Hapi Server is Started and All the Plugins are added');
  })
  .catch((e) => {
    console.log(`Error:${e}`);
  });
