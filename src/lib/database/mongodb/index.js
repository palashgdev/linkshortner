const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// connection string 172.21.0.2
mongoose.connect('mongodb://172.21.0.2:27017/Shortner');

mongoose.connection.on('error', (error) => {
  console.error(`Mongoose Connection Error : ${error}`);
  throw error;
});

console.log('MongoDB is now connected');
