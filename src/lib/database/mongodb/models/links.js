const mongo = require('mongoose');

mongo.Promise = global.Promise;

/* eslint-disable */
const Schema = mongo.Schema;
/* eslint-enable */

const link = new Schema(
  {
    username: {
      type: String,
      required: true,
    },
    link: {
      type: String,
      required: true,
    },
    shortenlink: {
      type: String,
      required: true,
      unique: true,
    },
    timestamps: {
      type: Date,
      default: Date.now,
      required: true,
    },
    console: {
      type: String,
      default: 'Linux',
    }, // chrome,safari
  },
  { collection: 'Link' },
);

export default mongo.model('linkShortner', link);
