const mongo = require('mongoose');

mongo.Promise = global.Promise;

/* eslint-disable */
const Schema = mongo.Schema;
/* eslint-enable */

const usersSchema = new Schema(
  {
    username: {
      type: 'String',
      required: true,
      unique: true,
    },
    name: {
      type: 'String',
      required: true,
    },
    password: {
      type: 'String',
      required: true,
    },
    email: {
      type: 'String',
      required: true,
    },
  },
  { collection: 'user', timestamp: 'true' },
);

export default mongo.model('users', usersSchema);
