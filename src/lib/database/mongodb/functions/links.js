/**
 * This file contain all the database function realted to links
 */
import os from 'os';

import { createHash } from './hash';
import Links from '../models/links';
import { getIdByUserName } from '../functions/users';

/**
 * This function is used to create the link Short
 * @async
 * @function createLink
 * @param {String} link
 * @param {String} id - id of the user
 * @param {String} length
 * @returns {Promise}
 *
 * @example
 * createLink({ link: 'https://www.google.com', length: 6 })
 * .then((e) => console.log(e))
 * .catch((e) => console.log(e));
 */

export async function createLink({ username, link, length }) {
  try {
    const user = getIdByUserName({ username });

    if (user.statusCode >= 300) {
      return {
        statusCode: 500,
        message: 'Something Went Wrong',
      };
    }
    const shortenlink = createHash(length);

    const timestamp = Date.now();

    const console = os.type();

    const query = new Links({
      username,
      link,
      shortenlink,
      timestamp,
      console,
    });

    const links = await query.save();

    return {
      statusCode: 201,
      message: 'Success',
      linkId: links._id,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      message: 'Database Server Error',
    };
  }
}
// createLink({
//   username: 'palashg7563',
//   link: 'https://www.google.xy',
//   length: 6,
// })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function delete the shorten link by it's id
 * @async
 * @function deleteLinkById
 * @param {String} id
 * @returns {Promise}
 * @example
 */

export async function deleteLinkById({ id }) {
  try {
    const query = Links.findOneAndRemove(id);

    if (!query) {
      return {
        statusCode: 400,
        message: 'Not Found',
      };
    }

    /* eslint-disable */
    const links = await query.exec();
    /* eslint-enable */

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      message: 'Database Server Error',
    };
  }
}

/**
 * This function is used to get a link of the user
 * @async
 * @function getLinkOfUser
 * @param {Stirng} id
 * @returns {Promise}
 * @example
 * getLinkOfUser({ id: '5ae219bf97a77c024fb8349d' })
 * .then((e) => console.log(e))
 * .catch((e) => console.log(e));
 */

export async function getLinkOfUser({ id }) {
  try {
    const query = Links.findById(id);

    if (!query) {
      return {
        statusCode: 400,
        message: 'Not Found',
      };
    }

    const links = await query.exec();

    return {
      statusCode: 200,
      message: 'Success',
      shortenlink: links.shortenlink,
    };
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      message: 'Database Server Error',
    };
  }
}

// getLinkOfUser({ id: '5afc9c07e8a88803187d0764' })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This database function will get all the links of the user
 * @async
 * @function getAllLinkOfUser
 * @param {String} username
 * @returns {Promise}
 *
 * @example
 * getAllLinkOfUser({ username: 'palashg7563' })
 * .then((e) => console.log(e))
 * .catch((e) => console.log(e));
 */
export async function getAllLinkOfUser({ username }) {
  try {
    const query = Links.find({ username });

    const links = await query.exec();

    return {
      statusCode: 200,
      message: 'Success',
      payload: links,
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: 500,
      message: 'Database Server Error',
    };
  }
}
// getAllLinkOfUser({ username: 'palashg7563' })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function is used to get a user's link by it's user's id
 * @async
 * @function getAllLinkByUserId
 * @param {String} userId
 * @returns {Promise}
 *
 * @example
 * getAllLinkByUserId({ userId: '5afca041639c88052e8e2a35' })
 *   .then((e) => console.log(e))
 *   .catch((e) => console.log(e));
 */
export async function getAllLinkByUserId({ userId }) {
  try {
    const query = Links.find({ _id: userId });

    const users = await query.exec();

    if (!users) {
      return {
        statusCode: 404,
        message: 'Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
      payload: users,
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: 500,
      message: 'Database Server Error',
    };
  }
}
// getAllLinkByUserId({ userId: '5afca041639c88052e8e2a35' })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function will delete the link by LongLink
 * @async
 * @function deleteLinkByLongLink
 * @param {Stirng} username
 * @returns {Promise}
 *
 * @example
 * deleteLinkByLongLink({ link: 'https://www.google.com' })
 * .then((e) => console.log(e))
 * .catch((e) => console.log(e));
 */
export async function deleteLinkByLongLink({ link }) {
  try {
    const query = Links.findOneAndRemove(link);

    const user = await query.exec();

    if (!user) {
      return {
        statusCode: 404,
        message: 'Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: 500,
      message: 'Database Server Error',
    };
  }
}
// deleteLinkByLongLink({ link: 'https://www.google.com' })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));

/**
 * This function will get a link by it's username
 * @async
 * @function getLinkByUserName
 * @param {String} username
 * @returns {Promise}
 * @example
 * getLinkByLongLink({ link: 'https://www.google.in' })
 *   .then((e) => console.log(e))
 *   .catch((e) => console.log(e));
 */
export async function getLinkByLongLink({ link }) {
  try {
    const query = Links.findOne({ link });

    const users = await query.exec();

    if (!users) {
      return {
        statusCode: 404,
        message: 'Not Found',
      };
    }

    return {
      statusCode: 200,
      message: 'Success',
      payload: { users },
    };
  } catch (error) {
    console.log(error);
    return {
      statusCode: 500,
      message: 'Database Server Error',
    };
  }
}
// getLinkByLongLink({ link: 'https://www.google.in' })
//   .then((e) => console.log(e))
//   .catch((e) => console.log(e));
