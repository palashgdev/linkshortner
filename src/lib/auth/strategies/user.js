import { getUserByUserId } from '../../database/mongodb/functions/users';

const TOKEN_KEY = process.env['TOKEN_KEY '];

export default {
  key: TOKEN_KEY,
  async validate(decode, request) {
    const { id: userId } = decode;

    getUserByUserId({ userId })
      .then(({ statusCode }) => {
        if (statusCode === 200) {
          return { isValid: true };
        }
        return { isValid: false };
      })
      .catch((error) => {
        console.log(error);
        return { isValid: false };
      });
  },
  verifyOptions: { algorithm: ['HS256'] },
};
