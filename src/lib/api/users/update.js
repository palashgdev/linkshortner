import joi from 'joi';
import { updateUserByUserName } from '../../database/mongodb/functions/users';

export default {
  description: 'This API will update the info of the user',
  notes: 'Only for user AUTH',
  tags: ['api', 'user'],
  validate: {
    payload: {
      name: joi.string().required(),
      email: joi.string().required(),
      username: joi.string().required(),
    },
  },
  async handler(request, h) {
    try {
      const { name, email, username } = request.payload;

      const res = await updateUserByUserName({ username, name, email });

      return h.response(res);
    } catch (error) {
      console.error(error);

      return {
        statusCode: 500,
        error: 'Server Error',
      };
    }
  },
};
