/**
 *
 */
import joi from 'joi';

import { getLinkOfUser } from '../../database/mongodb/functions/links';
import { getUserByUserId } from '../../database/mongodb/functions/users';

export default {
  description: 'This API is used to get a link of user',
  notes: 'Id and Token must be of user',
  tags: ['api', 'link'],
  validate: {
    params: joi
      .object({
        linkId: joi.string().required(),
        userId: joi.string().required(),
      })
      .min(2)
      .max(2),
  },
  async handler(request, h) {
    try {
      const { linkId, userId } = request.param;

      const res1 = await getUserByUserId({ userId });

      if (res1.statusCode !== 200) {
        return h.response({
          statusCode: 404,
          message: 'Something Goes Wrong',
        });
      }

      const res = await getLinkOfUser({ linkId });

      return h.response(res);
    } catch (error) {
      console.error(error);

      return {
        statusCode: 500,
        error: 'Server Error',
      };
    }
  },
};
