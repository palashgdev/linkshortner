import joi from 'joi';

import { deleteLinkByLongLink } from '../../database/mongodb/functions/links';
import { getUserByUserId } from '../../database/mongodb/functions/users';

export default {
  description: 'This API is used to delete the link of a user',
  notes: 'id and token must be of user',
  tags: ['api', 'link'],
  validate: {
    payload: joi
      .object({
        link: joi.string().required(),
      })
      .min(1)
      .max(1),
    params: joi
      .object({
        userId: joi.string().required(),
      })
      .min(1)
      .max(1),
  },
  async handler(request, h) {
    try {
      const { link } = request.payload;

      const { userId } = request.params;

      const res1 = await getUserByUserId({ userId });

      if (res1.statusCode !== 200) {
        return h.response({
          statusCode: 404,
          message: 'Something Goes Wrong',
        });
      }

      const res = await deleteLinkByLongLink({ link });

      return h.response(res);
    } catch (error) {
      console.error(error);

      return {
        statusCode: 500,
        error: 'Server Error',
      };
    }
  },
};
