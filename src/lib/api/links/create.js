import joi from 'joi';

import { createLink } from '../../database/mongodb/functions/links';
import { getUserByUserId } from '../../database/mongodb/functions/users';

export default {
  description: 'This API is used to create the link of a user',
  notes: 'Id and Token must be of user',
  tags: ['api', 'link'],
  validate: {
    payload: joi
      .object({
        username: joi
          .string()
          .strict()
          .required(),
        link: joi
          .string()
          .strict()
          .required(),
        length: joi
          .number()
          .min(3)
          .max(7),
      })
      .min(3)
      .max(3),
    params: joi
      .object({
        userId: joi.string().required(),
      })
      .min(1)
      .max(1),
  },
  async handler(request, h) {
    try {
      const { username, link } = request.payload;
      const { userId } = request.params;
      const length = 11;

      const res1 = await getUserByUserId({ userId });

      if (res1.statusCode !== 200) {
        return h.response({
          statusCode: 404,
          message: 'Something Goes Wrong',
        });
      }

      const res = await createLink({ username, link, length });

      return h.response(res);
    } catch (error) {
      console.error(error);

      return {
        statusCode: 500,
        error: 'Server Error',
      };
    }
  },
};
